terraform {
  backend "s3" {
    bucket = "s3-kel-11"
    region = "ap-northeast-1"
    key = "data/terraform-11"
    access_key = $AWS_ACCESS_KEY_ID
    secret_key = $AWS_SECRET_ACCESS_KEY
  }
}
