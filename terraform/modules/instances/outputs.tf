output "ip_cache" {
  value       = aws_instance.web-cache.*.public_ip
  description = "Public DNS name for Web Server Cache instance"
}

output "ip_cms" {
  value       = aws_instance.cms.public_ip
  description = "Public DNS name for CMS instance"
}

output "ip_lb-db" {
  value       = aws_instance.lb-db.private_ip
  description = "Public DNS name for Load Balancer DB instance"
}

output "ip_db" {
  value       = aws_instance.db.*.private_ip
  description = "Public DNS name for DB instance"
}

output "ip_bastion" {
  value       = aws_instance.bastion.public_ip
  description = "Public DNS name for Bastion instance"
}

output "ip_thumbor" {
  value       = aws_instance.thumbor.public_ip
  description = "Public DNS name for Thumbor instance"
}

output "ip_elk" {
  value       = aws_instance.elk.public_ip
  description = "Public DNS name for ELK instance"
}

output "alb_web_dns" {
  description = "DNS name of ALB"
  value       = aws_lb.alb.dns_name
}
