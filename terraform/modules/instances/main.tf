#################
# ALB
#################
resource "aws_lb" "alb" {
  name               = "Load-Balancer-Web"
  security_groups    = [var.sg.sg1, var.sg.sg5]
  subnets            = var.sub_pub.*.id
  load_balancer_type = "application"

  tags = merge(
   {
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
   },
   var.tags
   )
}

#################
# Target Group
#################
resource "aws_lb_target_group" "group" {
  name     = "Target-Group-HTTPS"
  port     = 443
  protocol = "HTTPS"
  vpc_id   = var.vpc.id
  stickiness {
    type = "lb_cookie"
  }
  health_check {
    path = "/"
    port = 443
  }
}

#################
# Listener
#################
resource "aws_lb_listener" "listener_https" {
  load_balancer_arn = "${aws_lb.alb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:ap-northeast-1:523919180211:certificate/ddb12f54-b68f-4250-8d71-2c405d15ef64"

  default_action {
    target_group_arn = "${aws_lb_target_group.group.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "listener_http" {
  load_balancer_arn = "${aws_lb.alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.group.arn}"
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
     }
   }
}

resource "aws_lb_target_group_attachment" "attachment" {
  count            = 2
  target_group_arn = "${aws_lb_target_group.group.arn}"
  target_id        = "${aws_instance.web-cache[count.index].id}"
  port             = 443
}

#################
# Create Instance
#################
resource "aws_instance" "web-cache" {
  count                  = length(var.sub_pub.*.id)
  ami                    = var.ami
  instance_type          = "t2.micro"
  key_name               = var.key

  vpc_security_group_ids = [var.sg.sg1]
  subnet_id              = var.sub_pub[count.index].id

  tags = merge(
   {
     Name        = "Web-Server-${count.index}",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "webserver"
   },
   var.tags
   )
  depends_on = [aws_instance.bastion, aws_instance.cms]
}

resource "aws_instance" "cms" {
   ami                    = var.ami
   instance_type          = "t2.micro"
   key_name               = var.key

   vpc_security_group_ids = [var.sg.sg1]
   subnet_id              = var.sub_pub[0].id

   tags = merge(
   {
     Name        = "CMS-Server",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "cms"
   },
   var.tags
   )
   
   provisioner "local-exec" {
     command = <<EOT
	echo public_ip_cms: ${aws_instance.cms.public_ip} >> ../ansible/group_vars/tag_Tier_webserver/main.yml
     EOT
   }
}

resource "aws_instance" "lb-db" {
   ami                    = var.ami
   instance_type          = "t3.micro"
   key_name               = var.key

   vpc_security_group_ids = [var.sg.sg6]
   subnet_id              = var.sub_pub[0].id

   tags = merge(
   {
     Name        = "Load-Balancer-DB",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "lb_db"
   },
   var.tags
   )
}

resource "aws_instance" "db" {
   count                  = length(var.sub_pvt.*.id)
   ami                    = var.ami
   instance_type          = "t2.micro"
   key_name               = var.key

   vpc_security_group_ids = [var.sg.sg2]
   subnet_id              = var.sub_pvt[count.index].id

   tags = merge(
   {
     Name        = "Database-${count.index}",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "database"
   },
   var.tags
   )
  depends_on = [aws_instance.bastion]
  provisioner "remote-exec" {
    inline = ["echo 'Waiting for server to be initialized...'"]

    connection {
      type        = "ssh"
      agent       = false
      host        = self.private_ip
      user        = var.ssh_user
      private_key = file(var.private_key_file_path)

      bastion_host        = aws_instance.bastion.public_ip
      bastion_private_key = file(var.private_key_file_path)
    }
  }

  provisioner "local-exec" {
    command = <<EOT
      ansible-playbook -i ${self.private_ip}, -u ubuntu --private-key ${var.private_key_file_path} \
        --ssh-common-args '-o ProxyCommand="ssh -q ubuntu@${aws_instance.bastion.public_ip} -i ${var.private_key_file_path} -W %h:%p"' ../ansible/db.yml
      
    EOT
  }
}

resource "aws_instance" "bastion" {
   ami                    = var.ami
   instance_type          = "t2.micro"
   key_name               = var.key

   vpc_security_group_ids = [var.sg.sg5]
   subnet_id              = var.sub_pub[0].id

   provisioner "file" {
     source      = "~/.ssh/key-11.pem"
     destination = var.private_key_file_path

     connection {
       type        = "ssh"
       user        = var.ssh_user
      # private_key = "${file("~/.ssh/key-11.pem")}"
       host        = aws_instance.bastion.public_ip
       insecure    = true
   }
  }

   user_data = <<-EOF
               #!/bin/bash
               chown ubuntu /home/ubuntu/.ssh/key-11.pem
               chgrp ubuntu /home/ubuntu/.ssh/key-11.pem
               chmod 600 /home/ubuntu/.ssh/key-11.pem
               apt-get update -y
               apt-get install ansible -y
               EOF

   tags = merge(
   {
     Name        = "Bastion",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "bastion"
   },
   var.tags
   )
}

resource "aws_instance" "thumbor" {
   ami                    = "ami-0adc2ab40ce72e19c"
   instance_type          = "t2.micro"
   key_name               = var.key

   vpc_security_group_ids = [var.sg.sg7]
   subnet_id              = var.sub_pub[0].id

   tags = merge(
   {
     Name        = "Thumbor",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "thumbor"
   },
   var.tags
   )
}

resource "aws_instance" "elk" {
   ami                    = var.ami
   instance_type          = "m4.large"
   key_name               = var.key

   root_block_device {
     volume_size = 40
   }

   vpc_security_group_ids = [var.sg.sg8]
   subnet_id              = var.sub_pub[0].id

   tags = merge(
   {
     Name        = "ELK",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "log"
   },
   var.tags
   )

   provisioner "local-exec" {
     command = <<EOT
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_cms/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_webserver/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_lb_db/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_thumbor/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_db/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_grafana/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_bastion/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_grafana/main.yml
        echo public_ip_elk: ${aws_instance.elk.public_ip} >> ../ansible/group_vars/tag_Tier_log/main.yml
     EOT
   }
}

resource "aws_instance" "grafana" {
   ami                    = var.ami
   instance_type          = "t2.micro"
   key_name               = var.key

   vpc_security_group_ids = [var.sg.sg9]
   subnet_id              = var.sub_pub[0].id

   tags = merge(
   {
     Name        = "Grafana-Server",
     Project     = var.project,
     Owner       = var.owner,
     Environment = var.environment
     Tier        = "grafana"
   },
   var.tags
   )
}
