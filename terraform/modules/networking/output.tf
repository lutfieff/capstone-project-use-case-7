output "sg" {
  value = {
    sg1     = aws_security_group.sg1.id
    sg2     = aws_security_group.sg2.id
    sg3     = aws_security_group.sg3.id
    sg4     = aws_security_group.sg4.id
    sg5     = aws_security_group.sg5.id
    sg6     = aws_security_group.sg6.id
    sg7     = aws_security_group.sg7.id
    sg8     = aws_security_group.sg8.id
    sg9     = aws_security_group.sg9.id
  }
}

output "vpc" {
  value       = aws_vpc.main
  description = "VPC ID"
}

output "public_subnet_ids" {
  value       = aws_subnet.public
  description = "List of public subnet IDs"
}

output "private_subnet_ids" {
  value       = aws_subnet.private
  description = "List of private subnet IDs"
}
