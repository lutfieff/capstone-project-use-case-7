data "aws_availability_zones" "available" {
  state = "available"
}

#################
# Creating VPC
#################
resource "aws_vpc" "main" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"
  enable_dns_hostnames = true

  tags = merge(
    {
      Name        = "Main-VPC-11",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# Public Subnet
#################
resource "aws_subnet" "public" {
  count = length(var.public_subnet_cidr_blocks)

  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr_blocks[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = "true"

  tags = merge(
    {
      Name        = "Subnet-Public-${count.index}",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# Private Subnet
#################
resource "aws_subnet" "private" {
  count = length(var.private_subnet_cidr_blocks)

  vpc_id     = aws_vpc.main.id
  cidr_block = var.private_subnet_cidr_blocks[count.index]
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = merge(
    {
      Name        = "Subnet-Private-${count.index}",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# Internet GW
#################
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name        = "Internet-Gateway",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# NAT GW
#################
resource "aws_eip" "nat" {
  vpc = true
  count = 2
}

resource "aws_nat_gateway" "main" {
  count = length(var.public_subnet_cidr_blocks)

  allocation_id     = aws_eip.nat[count.index].id
  subnet_id         = aws_subnet.public[count.index].id

  tags = merge(
    {
      Name        = "NAT-Gateway-${count.index}",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# RT Public
#################
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name        = "Route-Table-Public",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# RTA Public
#################
resource "aws_route_table_association" "rta_public" {
  count = length(var.public_subnet_cidr_blocks)

  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

#################
# Route Public
#################
resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

#################
# RT Private
#################
resource "aws_route_table" "private" {
  count = length(var.private_subnet_cidr_blocks)

  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name        = "Route-Table-Private",
      Project     = var.project,
      Owner       = var.owner,
      Environment = var.environment
    },
    var.tags
  )
}

#################
# RTA Private
#################
resource "aws_route_table_association" "rta_private" {
  count = length(var.private_subnet_cidr_blocks)

  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

#################
# Route Private
#################
resource "aws_route" "private" {
  count = length(var.private_subnet_cidr_blocks)

  route_table_id         = aws_route_table.private[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main[count.index].id
}

#################
# Security Group
#################
resource "aws_security_group" "sg1" {
name        = "allow_https_ssh_icmp"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_icmp"
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_https_icmp_ssh"
  }
}

resource "aws_security_group" "sg2" {
name        = "allow_ssh_mysql"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_mysql"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_mysql_ssh"
  }
}


resource "aws_security_group" "sg3" {
name        = "allow_bastion_os"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_bastion_os"
  }
}

resource "aws_security_group" "sg4" {
name        = "allow_ssh"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "sg5" {
name        = "allow_http_ssh_icmp"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_icmp"
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_http_icmp_ssh"
  }
}

resource "aws_security_group" "sg6" {
name        = "allow_proxysql"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_mysql"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_proxysql1"
    from_port   = 6032
    to_port     = 6032
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_proxysql2"
    from_port   = 6033
    to_port     = 6033
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_ssh_6033_6032"
  }
}

resource "aws_security_group" "sg7" {
name        = "allow_thumbor"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_8888"
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_8125"
    from_port   = 8125
    to_port     = 8125
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_8001"
    from_port   = 8001
    to_port     = 8001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_8002"
    from_port   = 8002
    to_port     = 8002
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_8003"
    from_port   = 8003
    to_port     = 8003
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_8004"
    from_port   = 8004
    to_port     = 8004
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_ssh_custom_thumbor"
  }
}

resource "aws_security_group" "sg8" {
name        = "allow_elk"
vpc_id     = aws_vpc.main.id
ingress {
    description = "Allow_mysql"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_kibana"
    from_port   = 5601
    to_port     = 5601
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_elastic"
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_logstash"
    from_port   = 5044
    to_port     = 5044
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_ssh_custom_elk"
  }
}

resource "aws_security_group" "sg9" {
name        = "allow_http_ssh_icmp_grafana"
vpc_id     = aws_vpc.main.id

ingress {
    description = "Allow_ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    description = "Allow_logstash"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_http_icmp_ssh_grafana"
  }
}
